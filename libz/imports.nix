z: libz:
rec
{
  # Check, if the directory has a default.nix file.
  # Such directories should be imported non-recursively.
  dirHasDefault = (
    dir:
    libz.any (
      name: name == "default.nix"
    ) (
      libz.attrNames (libz.readDir dir)
    )
  );

  # Convert nix file path to a "module name"
  filenameToModule = (
    filename:
    libz.removeSuffix ".nix" (
      libz.baseNameOf (
        libz.toString
        filename
      )
    )
  );

  # Iterate over importable paths in the directory
  readModuleDir = (
    dir:
    libz.attrNames (
      libz.filterAttrs (
        path: type: (
          # Regular .nix file
          (libz.hasSuffix ".nix" path) &&
          # Except default.nix
          path != "default.nix"
        ) || (
          # Submodule (directory)
          type == "directory"
        )
      ) (
        libz.readDir dir
      )
    )
  );

  recReducer = libz.listToAttrs;
  flatReducer = libz.concatMap ({name, value}: value);
  recImporterFn = fn: leaf: fn (import leaf z);
  flatImporterFn = fn: leaf: [ (fn (import leaf z)) ];

  # Given a module path path, recursively walk the submodule tree,
  # import every lead node and reduce the values in intermediate nodes
  #
  # Same as a "plain" import expression, but it will recursively
  # import all files and subdirectories into an attrset,
  # if no "default.nix" file is present:
  # {
  #   foo = import dir/foo.nix;
  #   bar = {
  #     baz1 = import dir/bar/baz1.nix; # <- dir/bar/default.nix doesn't exist
  #     baz2 = import dir/bar/baz2.nix; #    manually recurse into dir/bar
  #   };
  #   boo = import dir/boo; # <- dir/boo/default.nix exists, don't recurse
  # }
  importPath = importPathFn libz.id;
  importPathArg = arg: importPathFn (value: value arg);
  importPathFn = fn: args: (importPathFn' fn args).value;
  importPath' = importPathFn' libz.id;
  importPathFn' = (
    fn:
    { path, reducer ? recReducer, importer ? recImporterFn fn }: let
      importDirRec = (
        dir:
        importDir {
          inherit reducer importer dir;
        }
      );
      isLeaf = !(libz.pathIsDirectory path) || (dirHasDefault path);
      importRec = if isLeaf then importer else importDirRec;
    in {
      name = filenameToModule path;
      value = importRec path;
    }
  );

  # Same as importPath, but forcefully imports the top level directory
  # as if it didn't have a "default.nix" file
  importDir = importDirFn libz.id;
  importDirArg = arg: importDirFn (value: value arg);
  importDirFn = (
    fn:
    { dir, reducer ? recReducer, importer ? recImporterFn fn }: let
      importPathRec = (
        path:
        importPath' {
          inherit reducer importer;
          path = (dir + "/${path}");
        }
      );
      module = readModuleDir dir;
    in (
      reducer (
        libz.map
        importPathRec
        module
      )
    )
  );

  # Same as importDir, but returns a flat list instead of a nested attrset
  flatImportDir = flatImportDirFn libz.id;
  flatImportDirArg = arg: flatImportDirFn (value: value arg);
  flatImportDirFn = (
    fn:
    { dir, reducer ? flatReducer, importer ? flatImporterFn fn }:
      importDir {inherit dir reducer importer;}
  );

  # Given a list of submodules, return a merged attrset that
  # contains all publically exported attributes.
  getExports = (
    target: submodules:
    libz.mapAttrsToList
    (
      name: sub:
      (sub.exports."${target}" or [])
    )
    submodules
  );

  export = target: flakes:
    let
      # Get the exports from all flakes (other than self)
      otherFlakes = libz.removeAttrs flakes ["self"];
      exports = getExports target otherFlakes;

      # Add the special "all" source that combines all flake exports
      combinedExports = libz.concatLists exports;
    in
      combinedExports;

}
