z: libz:
{
  join = libz.fun.mkFun "_" {
    _ = libz.strings.concatStrings;
    sep = libz.strings.concatStringsSep;
  };

  has = libz.fun.mkFun "substring" {
    prefix = libz.hasPrefix;
    suffix = libz.hasSuffix;
    substring = libz.hasInfix;
    match = expr: string: libz.match ".*${expr}.*" string != null;
  };

  remove = let
    _removeWithCheck = (
      kind: remove: str:
      assert libz.string.has.${kind} remove str;
      libz.string.remove.${kind} remove str
    );
  in {
    prefix = libz.removePrefix;
    suffix = libz.removeSuffix;

    prefix' = _removeWithCheck "prefix";
    suffix' = _removeWithCheck "suffix";
  };
}
