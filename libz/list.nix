z: libz:
{
  concat = libz.concatLists;
  toAttrs = libz.listToAttrs;
  enumerate = libz.imap0;

  map = libz.fun.mkFun "f" {
    f = libz.map;
    concat = libz.concatMap;
    toValues = libz.flip libz.genAttrs;
    toItems = f: n: libz.list.toAttrs (map f n);
  };
}
