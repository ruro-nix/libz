z: libz:
rec {
  # Make the set into a functor by delegating function calls to attribute attr
  mkFunctor = attr: set: set // { __functor = set.${attr}; };

  # Like mkFunctor, but the delegated function won't have the "self" argument
  mkFun = attr: set: set // { __functor = libz.const set.${attr}; };

  # Like flip, but the list of functions comes first
  compose = libz.flip libz.pipe;

  # Like toString but with pretty indentation and stuff
  fmt = libz.generators.toPretty {};
}
