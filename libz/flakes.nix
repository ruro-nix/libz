z: libz:
{
  autoExport = path: name: kind:
    let
      dir = path + "/${kind}";
      hasExport = libz.pathExists dir;
      importer = dir: libz.imports.flatImportDir {inherit dir;};
      export = (
        if hasExport
        then {"${kind}" = importer dir;}
        else {}
      );
    in
      export;

  autoExports = path: name:
    let
      kinds = [
        "libOverlays"
        "pkgOverlays"
        "nixosModules"
      ];
      autoExport = libz.flakes.autoExport path name;
      exportList = libz.list.map autoExport kinds;
      exports = libz.attr.concat exportList;
    in
      exports;

  getOverlays = kind: inputs:
    let
      autoExports = libz.imports.export "auto" inputs;
      importer = path:
        let
          dir = path + "/${kind}";
          hasExport = libz.pathExists dir;
          export = libz.imports.flatImportDir {inherit dir;};
          checked = if hasExport then export else [];
        in
          checked;
      overlays = libz.concatMap importer autoExports;
    in
      overlays;
}
