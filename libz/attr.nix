z: libz:
{
  toList = libz.attr.map.toList libz.id;
  get = set: name: libz.attr.item name set.${name};
  concat = libz.foldl libz.mergeAttrs {};
  filter = libz.filterAttrs;
  remove = set: names: libz.removeAttrs names set;
  pick = libz.getAttrs;

  item = libz.nameValuePair;
  items = libz.attr.toList;

  map = libz.fun.mkFun "f1" {
    f1 = libz.mapAttrs';
    f2 = nf: vf: libz.attr.map.f1 (n: v: libz.attr.item (nf n) (vf v));
    toValues = libz.mapAttrs;
    toList = libz.mapAttrsToList;
  };

  names = libz.fun.mkFun "toList" {
    filter = f: libz.attr.filter (n: v: f n);
    toList = libz.attrNames;

    map = libz.fun.mkFun "toItems" {
      toItems = f: libz.attr.map.f2 f libz.id;
      toList = f: libz.compose [ libz.attr.names.toList (libz.list.map f) ];
    };
  };

  values = libz.fun.mkFun "toList" {
    filter = f: libz.attr.filter (n: v: f v);
    toList = libz.attrValues;
    get = libz.getAttr;

    map = libz.fun.mkFun "toItems" {
      toItems = f: libz.attr.map.f2 libz.id f;
      toList = f: libz.compose [ libz.attr.values.toList (libz.list.map f) ];
    };
  };
}
