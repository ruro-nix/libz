z:
let
  inherit (z.inputs.upstream-nixpkgs) lib;
  otherInputs = builtins.removeAttrs z.inputs ["libz"];

  # Enforce deprecation warnings
  warningsOverlay = (
    _: _:
    {
      zip = null;
      showVal = null;
      readPathsFromFile = null;
      nixpkgsVersion = null;
      mkFixStrictness = null;
      literalExample = null;
      crossLists = null;
    }
  );

  # Initialize with standard nix builtins + libraries
  builtinsOverlay = (
    _: _:
    builtins
  );

  # Bootstrap the libz importing functions
  importsOverlay = (
    _: prev:
    {imports = import ./imports.nix z prev;}
  );

  # Import all libz submodules (recursively resolves against final)
  libzOverlay = (
    final: prev:
    prev.imports.importDirArg final {dir = ./.;}
  );

  # Export all the "extra" overlays exported by other flakes
  extraOverlay = (
    final: prev:
    lib.foldl
    (l: r: l // (r final prev))
    {}
    (prev.flakes.getOverlays "libOverlays" otherInputs)
  );

  # We use the non-recursive version of prev here, because the
  # stdlib can't handle overlays with dynamic attribute names
  applyOverlays = lib.foldl (
    prev: over:
    prev.extend
    (
      final: prevRec:
      let
        diff = over.value final prev;
        prevStages = prevRec.stages or {};
        stage."${over.name}" = diff;
        stages = prevStages // stage;
      in
        diff // {inherit stages;}
    )
  );

  # Apply all the local overlays
  localOverlays = [
    {name = "warnings"; value = warningsOverlay;}
    {name = "builtins"; value = builtinsOverlay;}
    {name = "imports"; value = importsOverlay;}
    {name = "final"; value = libzOverlay;}
    {name = "extra"; value = extraOverlay;}
  ];
  finalLibz = applyOverlays lib localOverlays;

  # Validate, that all exported attributes can be resolved
  # without errors and don't have any cyclic depdendencies
  stages = finalLibz.stages;
  libz = finalLibz.deepSeq stages finalLibz;

  # ToDo: replace with traceVerbose when it gets released
  # libz = finalLibz.trace (finalLibz.fun.fmt stages) finalLibz;
in
  libz
